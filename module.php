<?php
class Sharewellmodule{
	
	/**
	* Updates permission role
	*
	* @param Google_DriveService $service Drive API service instance.
 	* @param String $fileId ID of the file to update permission for.
 	* @param String $email Emailaddress of the permission to update.
 	* @param String $newRole The value "owner", "writer" or "reader".
 	* @param Array $AdditionalRoles empty or Array with only value "commenter".
 	* 
 	* Note:: To change from owner or write to commneter, $newRole value should be "reader"
 	* with $AdditionalRole as Array with value "commentor"
	*/
	
	public function updatePermission($service, $fileId, $email, $newRole, $AdditionalRoles) {
		try {
    		$permissionId = $this->getIDfromEmail($service,$email);
    		$permission = $service->permissions->get($fileId, $permissionId);
    		$permission->setRole($newRole);
    		//print_r($permission);
    		$permission->setAdditionalRoles($AdditionalRoles);
    		$service->permissions->update($fileId, $permissionId, $permission);
    		echo "Update role for permission ",$permissionId," in file ",$fileId,'</br>';
  		} catch (Exception $e) {
    		echo "An error occurred: " , $e->getMessage();
  		}
  	}

	/**
 	* Gets Permission ID for an email address. 
 	*
 	* @param Google_DriveService $service Drive API service instance.
 	* @param String $email Email address to retrieve ID for.
 	*
	*/

  	public function getIDfromEmail($service,$email){
  		try {
    		$permissionId = $service->permissions->getIdForEmail("bishalgrg@hotmail.co.uk");
    		return $permissionId->getId();    
  		} catch (Exception $e) {
    		echo "An error occurred: " , $e->getMessage();
    		return null;    
  		}
	}

	/**
	* Update file to Downloadable or Not Downloadable
	* @param Google_DriveService $service Drive API service instance.
	* @param String $fileId ID of the file to update
	* @param boolean $value true or false (true to stop download) 
	* 
	* Note:: It does not work with google files and works on if the role is reader
	*/
	public function updaterestriction($service,$fileId,$value){
		try{
		    //$fileId = "0B-ZjiCjGQAFdSVg0WkNCdGhZc2c";
		    $file = $service->files->get($fileId);

		    $label = $file->getlabels();
		    $label->setRestricted($value);
		    $file->setlabels($label);
		 
		    $parameters = array(
		        'newRevision' => false,
		    );
		    $service->files->update($fileId,$file,$parameters);
		    echo "Restriction updated for file id ",$fileId,'</br>';
	  	} catch (Exception $e){
	    	echo "An error occurred: " , $e->getMessage();    
	  	}
	}


	public function retrieveAllFiles($service) {
		try{
			$file = $service->files->listFiles(array())->getItems();

    		foreach ($file as $f) {
		      $id = $f->getID();
		      $restriction = $f->getlabels()->getrestricted();
		      $title = $f->getTitle();

		      echo '<b>', "File id: ",'</b>', $id, '<b>'," Details:",'</b>','</br>';
		      echo "Not able to download :",$restriction,"</br>";
		      echo "Title : ",$title,"</br>";
		      echo '<b>',"Permissions for file id: ",'</b>',$id,"</br>";
		      $this->retrievePermissions($service,$id);
		      echo "</br>";
		    }
  		} catch (Exception $e){
    		echo "An error occurred: " , $e->getMessage();
  		}
	}

	public function retrievePermissions($service, $fileId) {
	  	try {
		    $permissions = $service->permissions->listPermissions($fileId)->getItems();
		    
		    foreach ($permissions as $p) {
		      $email = print_r($p->emailAddress,true);
		      $id = $p->getID();
		      $role = $p->getRole();
		      $commentor = $p->getAdditionalRoles();
		      echo '<b>',"Permission id: ",'</b>',$id;
		      echo '<b>',"	email : ",'</b>',$email;
		      echo '<b>'," 	Role: ",'</b>',$role;
		      echo '<b>'," 	Additional role: ",'</b>',$commentor[0]."</br>";
		    }
	  	} catch (Exception $e) {
	  		echo "An error occurred: " , $e->getMessage();
	  	}
	}

	public function removepermission($service,$fileId,$permissionId){
	  	try {
	    	$service->permissions->delete($fileId,$permissionId);
	    	echo "Permission : " , $permissionId , " Deleted from file ", $fileId,'</br>';    
	  	} catch (Exception $e) {
	    	echo "An error occurred: ",$e->getMessage();    
	  	}
	}

	public function insertpermission($service,$fileId,$email,$role,$type){
  		try {
		    //$fileId = "0B-ZjiCjGQAFdSVg0WkNCdGhZc2c";
		    $newPermission = new Google_Permission();
		    $newPermission->setValue($email);
		    $newPermission->setType($type);
		    $newPermission->setRole($role);
		    $service->permissions->insert($fileId, $newPermission);
		    echo "Permission to ",$email," added to file ",$fileId,'</br>';
	  	} catch (Exception $e) {
    		echo "An error occurred: " , $e->getMessage();    
  		}
	}
}
?>